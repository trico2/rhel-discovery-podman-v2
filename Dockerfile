FROM registry.access.redhat.com/ubi8/ubi-minimal:latest
LABEL maintainer="samander@redhat.com"

# Expose the port used by Gunicorn
EXPOSE 8080

# Start execution as root
USER 0

# Update packages and then install requirements
RUN microdnf -y update && microdnf install -y python3 gpgme-devel openssh-clients openssl

# Add base files and move them into place
RUN mkdir -p /var/tmp/base
ADD reqs/app.tar.gz /var/tmp/base
RUN /bin/bash /var/tmp/base/setup.sh && rm -rf /var/tmp/base

# Upgrade the pip module before using and install wheel before dependencies
RUN python3 -m pip install --upgrade pip && python3 -m pip install wheel && python3 -m pip install -r /var/tmp/requirements.txt

# Create other directories/mounts for use later and fix permissions, including making the passwd
# file work with SSH
RUN mkdir -p /var/log /var/data /var/tmp/ansible /sshkeys /var/tmp/ssl && chown -R 1001:1001 /var/log /var/data /var/tmp/ansible /sshkeys /var/tmp/ssl \
    && chmod g=u /etc/passwd

# Change to the non-root user
USER 1001

# Set required environmental variables
ENV DJANGO_LOG_HANDLERS=console,file
ENV DJANGO_LOG_FORMATTER=verbose
ENV DJANGO_DEBUG=False
ENV DJANGO_SECRET_PATH=/var/data/secret.txt
ENV DJANGO_LOG_LEVEL=INFO
ENV DJANGO_LOG_FILE=/var/log/app.log
ENV PRODUCTION=True
ENV DJANGO_DB_PATH=/var/data/
ENV PYTHONHASHSEED=0
ENV ANSIBLE_LOCAL_TEMP=/var/tmp/ansible
ENV HOME=/var/tmp/ansible

ENTRYPOINT ["/bin/bash", "/var/tmp/entrypoint.sh"]
CMD ["/bin/bash", "/deploy/docker_run.sh"]