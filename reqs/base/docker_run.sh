#!/usr/bin/env bash
eval `ssh-agent -s`

if [[ ${RUN_LOCAL,,} = "true" ]]; then
    echo "Generating temporary SSL certs"
    bash /deploy/generate_ssl_certs.sh
fi

if [[ ${USE_SUPERVISORD,,} = "false" ]]; then
    echo "Running without supervisord"
    bash /deploy/server_run.sh
else
    echo "Running with supervisord"
    /usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf
fi
