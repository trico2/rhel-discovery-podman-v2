#!/usr/bin/env bash

cd /var/tmp/ssl

cat << EOF > ca.cnf
[ req ]
prompt = no
encrypt_key = no
default_bits = 4096
default_md = sha256
distinguished_name = dn
x509_extensions = v3_req

[ dn ]
C = US
ST = North Carolina
L = Raleigh
O = Red Hat
OU = Product Discovery
CN = Product Discovery Temporary Root

[ v3_req ]
keyUsage = critical,keyCertSign,cRLSign
basicConstraints = critical,CA:true
subjectKeyIdentifier = hash
EOF

openssl req -x509 -sha256 -days 3650 -newkey rsa:4096 -config ca.cnf \
    -keyout ca.key -out ca.crt

cat << EOF > server.cnf
[ req ]
prompt = no
encrypt_key = no
default_bits = 4096
default_md = sha256
distinguished_name = dn
x509_extensions = v3_req

[ dn ]
C = US
ST = North Carolina
L = Raleigh
O = Red Hat
OU = Product Discovery
CN = discovery-console.redhat.local

[ v3_req ]
keyUsage = digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth
basicConstraints = critical,CA:false
subjectKeyIdentifier = hash
subjectAltName = @alt_names

[ alt_names ]
DNS.1 = discovery-console.redhat.local
EOF

openssl genrsa -out server.key 2048
openssl req -new -key server.key -out server.csr -config server.cnf
openssl x509 -req -in server.csr -CA ca.crt \
    -CAkey ca.key -CAcreateserial -out server.crt \
    -days 730 -extfile server.cnf -extensions v3_req