#!/usr/bin/env bash

# server-migrate
python3 /usr/lib/python3.6/site-packages/discovery-server/manage.py migrate --settings quipucords.settings -v 3 || exit 1;
# server-set-superuser
cat /deploy/setup_user.py | python3 /usr/lib/python3.6/site-packages/discovery-server/manage.py shell --settings quipucords.settings -v 3 || exit 1;

if [[ ${USE_SUPERVISORD,,} = "false" ]]; then
    cd /usr/lib/python3.6/site-packages/discovery-server
fi

gunicorn quipucords.wsgi -c /deploy/gunicorn.conf.py
