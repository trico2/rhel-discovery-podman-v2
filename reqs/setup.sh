#!/bin/bash

cd /var/tmp/base

# Install sshpass
rpm -ivh /var/tmp/base/sshpass-1.06-3.el8ae.x86_64.rpm

# Move the directory-server files into place
mv discovery/* /usr/lib/python3.6/site-packages/

# Setup the 'bin' package
mkdir -p /usr/lib/python3.6/site-packages/bin
mv timeout_ssh /usr/lib/python3.6/site-packages/bin/timeout_ssh
chmod +x /usr/lib/python3.6/site-packages/bin/timeout_ssh

# Add the main scripts
mkdir -p /deploy
mv base/* /deploy/
chown -R 1001:1001 /deploy

# Move the pip requirements to the right spot
mv requirements.txt /var/tmp/requirements.txt

# Setup the entrypoint
mv entrypoint.sh /var/tmp/entrypoint.sh
chmod +x /var/tmp/entrypoint.sh
chown 1001:0 /var/tmp/entrypoint.sh