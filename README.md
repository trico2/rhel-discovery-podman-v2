# rhel-discovery

Evolution of the SEAP Discovery Tool, designed for use on OpenShift/OKD clusters.

## Running the Discovery Tool in an OpenShift Cluster

### Prerequisites
1. Access to quay.io/redhat_emp1 and ability to generate a pull secret
2. OpenShift 4+
3. The `oc` client installed
4. The OpenShift cluster can dynamically provision PVs for PVCs

### Procedure
1. Create a new namespace, for example: `oc new-project rhel-discovery`
2. Add the pull secret from Quay: `oc create -f pull-secret-here.yml -n rhel-discovery`
3. Add the template to the project `oc create -f discovery-template.yml -n rhel-discovery`
4. Switch to the new project (in case `oc new-project` didn't already do this): `oc project rhel-discovery`
5. Use the template: `oc new-app rhel-discovery-template -p OCP_URL=apps.your.ocp.cluster -p PULL_SECRET=your-pull-secret`

A new route will be created [https://discovery-console.apps.your.ocp.cluster](https://discovery-console.apps.your.ocp.cluster). The default login is: `admin` with password `dscpassw0rd`.

### Notes
A number of variables can be changed within the template, including Postgres username/password, Ansible log level, etc. Please review `discovery-template.yml` to make adjustments.

## Building the images

### Prerequisites
1. Docker/Podman is installed
2. `make` is installed

### Procedure
1. Build the image: `make build`
2. Test the build: `make test`
3. Cleanup the test: `make clean-test`
4. Use the image produced
