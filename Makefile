.PHONY: all test-docker test-podman clean-docker clean-podman clean build

DOCKER=docker
PODMAN=podman
TAR=bsdtar
SLEEP=sleep
RM=rm

all:
	$(TAR) -czvf reqs/app.tar.gz -C reqs base discovery entrypoint.sh requirements.txt sshpass-1.06-3.el8ae.x86_64.rpm timeout_ssh setup.sh
	$(DOCKER) build . -t quay.io/redhat_emp1/rhel-discovery:latest
	$(DOCKER) push quay.io/redhat_emp1/rhel-discovery:latest

build:
	$(TAR) -czvf reqs/app.tar.gz -C reqs base discovery entrypoint.sh requirements.txt sshpass-1.06-3.el8ae.x86_64.rpm timeout_ssh setup.sh
	$(DOCKER) build . -t quay.io/redhat_emp1/rhel-discovery:latest

test-docker:
	$(DOCKER) network create dsc-net
	$(DOCKER) run --name dsc-db --network=dsc-net --publish 5432:5432 -e POSTGRESQL_USER=postgres -e POSTGRESQL_PASSWORD=dscpassw0rd \
		-e POSTGRESQL_DATABASE=dsc-db -d registry.redhat.io/rhel8/postgresql-13:latest
	$(SLEEP) 10
	$(DOCKER) restart dsc-db
	$(SLEEP) 15
	$(DOCKER) run --name discovery --network=dsc-net --publish 8080:8080 -e QPC_SERVER_TIMEOUT=120 -e QPC_DBMS_USER=postgres \
		-e QPC_DBMS_PASSWORD=dscpassw0rd -e ANSIBLE_LOG_LEVEL=0 -e NETWORK_CONNECT_JOB_TIMEOUT=600 -e NETWORK_INSPECT_JOB_TIMEOUT=10800 \
		-e QPC_SERVER_USERNAME=admin -e QPC_SERVER_USER_EMAIL=admin@example.com -e QPC_SERVER_PASSWORD=dscpassw0rd -e QPC_DBMS_HOST=dsc-db \
		-e USE_SUPERVISORD=false -e DJANGO_LOG_HANDLERS=console,file -e DJANGO_LOG_FORMATTER=verbose -e DJANGO_DEBUG=False \
		-e DJANGO_SECRET_PATH=/var/data/secret.txt -e DJANGO_LOG_LEVEL=INFO -e DJANGO_LOG_FILE=/var/log/app.log \
		-e PRODUCTION=True -e DJANGO_DB_PATH=/var/data/ -e PYTHONHASHSEED=0 -e ANSIBLE_LOCAL_TEMP=/var/tmp/ansible \
		-e ANSIBLE_LOG_LEVEL=3 -e RUN_LOCAL=true -d quay.io/redhat_emp1/rhel-discovery:latest

test-podman:
	$(PODMAN) run --name dsc-db --pod new:discovery-pod --publish 8080:8080 -e POSTGRESQL_USER=postgres -e POSTGRESQL_PASSWORD=dscpassw0rd \
		-e POSTGRESQL_DATABASE=dsc-db -d registry.redhat.io/rhel8/postgresql-13:latest
	$(SLEEP) 10
	$(PODMAN) restart dsc-db
	$(SLEEP) 15
	$(PODMAN) run --name discovery --pod discovery-pod -e QPC_SERVER_TIMEOUT=120 -e QPC_DBMS_USER=postgres \
		-e QPC_DBMS_PASSWORD=dscpassw0rd -e ANSIBLE_LOG_LEVEL=0 -e NETWORK_CONNECT_JOB_TIMEOUT=600 -e NETWORK_INSPECT_JOB_TIMEOUT=10800 \
		-e QPC_SERVER_USERNAME=admin -e QPC_SERVER_USER_EMAIL=admin@example.com -e QPC_SERVER_PASSWORD=dscpassw0rd -e QPC_DBMS_HOST=localhost \
		-e USE_SUPERVISORD=false -e DJANGO_LOG_HANDLERS=console,file -e DJANGO_LOG_FORMATTER=verbose -e DJANGO_DEBUG=False \
		-e DJANGO_SECRET_PATH=/var/data/secret.txt -e DJANGO_LOG_LEVEL=INFO -e DJANGO_LOG_FILE=/var/log/app.log \
		-e PRODUCTION=True -e DJANGO_DB_PATH=/var/data/ -e PYTHONHASHSEED=0 -e ANSIBLE_LOCAL_TEMP=/var/tmp/ansible \
		-e ANSIBLE_LOG_LEVEL=3 -e RUN_LOCAL=true -d quay.io/redhat_emp1/rhel-discovery:latest

clean-docker:
	-$(DOCKER) kill discovery
	-$(DOCKER) kill dsc-db
	-$(DOCKER) rm discovery
	-$(DOCKER) rm dsc-db
	-$(DOCKER) network rm dsc-net

clean-podman:
	-$(PODMAN) kill discovery
	-$(PODMAN) kill dsc-db
	-$(PODMAN) rm discovery
	-$(PODMAN) rm dsc-db
	-$(PODMAN) pod rm discovery-pod

clean:
	-$(DOCKER) image rm quay.io/redhat_emp1/rhel-discovery:latest
	-$(RM) -f reqs/app.tar.gz